""" BIENVENIDOS AL BOT PARA EL GRUPO DE TELEGRAM DE PYTHON ESPAÑA, TOMEN ASIENTO Y DISFRUTEN!
"""


from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging as log
import json

TOKEN = "TOKEN"

log.basicConfig(level=log.DEBUG,
                format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

updater = Updater(TOKEN)
dispatcher = updater.dispatcher

command_list = ["AddLink", "GetLinks", "Help"]

# ----------------- BOT FUNCTIONS --------------------------------------------------------------------------------------
def new_user(bot, update):
    user_name = update.message.from_user.name
    update.message.reply_text("Bienvenido %s al grupo de Python España!\n\n" % str(user_name) +
                              "Esperamos tus preguntas/dudas tanto como tú una respuesta!\n"
                              "Esta comunidad esta hecha para aprender unos de los otros. "
                              "No tengas pudor en preguntar. Cualquier tema On-Topic será bienvenido!\n\n"
                              "Puedes abrirme un privado y escribir /help para más informacion.\n\n"
                              "Admins : @astrojuanlu @aaloy\n\n"
                              "Antes de nada échale un vistazo a estas páginas :\n\n "
                              "- Python ESP GitHub : https://github.com/python-spain/PMF\n"
                              "- Preguntas inteligentes : http://www.sindominio.net/ayuda/preguntas-inteligentes.html\n")


# COMMANDS FUNCTIONS
def test(bot, update):
    user_name = update.message.from_user.name
    update.message.reply_text("Bienvenido %s al grupo de Python España!\n\n" %str(user_name) +
                              "Esperamos tus preguntas/dudas tanto como tú una respuesta!\n"
                              "Esta comunidad esta hecha para aprender unos de los otros. "
                              "No tengas pudor en preguntar. Cualquier tema On-Topic será bienvenido!\n\n"
                              "Puedes abrirme un privado y escribir /help para más informacion.\n\n"
                              "Admins : @astrojuanlu @aaloy\n\n"
                              "Antes de nada échale un vistazo a estas páginas :\n\n "
                              "- Python ESP GitHub : https://github.com/python-spain/PMF\n"
                              "- Preguntas inteligentes : http://www.sindominio.net/ayuda/preguntas-inteligentes.html\n")


def add_link(bot, update):
    user = update.message.from_user.name
    text = update.message.text
    text = text.split("'")
    link = text[2]
    page_name = text[1]
    string = " has added : \n%s %s" %(page_name, link)

    with open('Pages.json', 'r') as input_stream:
        try:
            data = json.load(input_stream)
        except ValueError:
            data = {}
    data[page_name] = link
    print(data)

    with open('Pages.json', 'w') as output_stream:
        json.dump(data, output_stream)

    update.message.reply_text(str(user) + string)


def get_link(bot, update):
    dic = None
    with open('Pages.json', 'r') as input_stream:
        try:
            dic = json.load(input_stream)
        except ValueError:
            dic = {}

    string = ""
    for key in dic.keys():
        string = string + "- %s : %s\n\n" % (key, dic[key])
    update.message.reply_text(string)


def help(bot, update):
    user = update.message.from_user.name
    string = "Buenas %s necesitas mi ayuda verdad? Dispara!\n\n" \
             "Lista de comandos :\n" \
             "- /AddLink : For adding an interesting link!\n" \
             "- /GetLinks : For getting a list of interesting links!\n" \
             "- /Admins : For getting a list of admins!\n" \
             "- /Help : I think you don't need more information\n\n" \
             "Se que no soy muy útil aún, recuerda que estoy en Alfa. Espero haberte ayudado!\n\n" \
             "Si te ha quedado alguna duda, no dudes en preguntar algo por el grupo!" % user

    update.message.reply_text(string)


def admins(bot, update):
    pass


# ------------ COMMANDS --------------
# Test
dispatcher.add_handler(CommandHandler("test", test))

# AddLink for adding an interesting page
dispatcher.add_handler(CommandHandler("AddLink" or "addlink", add_link))

# GetLinks for getting the list of links inside the 'Pages.json' file
dispatcher.add_handler(CommandHandler("GetLinks" or "getlinks" or "Links" or "links", get_link))

# Admins fot getting the list of admins in the group
dispatcher.add_handler(CommandHandler("Admins" or "admins", admins))
# Help
dispatcher.add_handler(CommandHandler("Help" or "help", help))

# -------------------------------------------------------------------- #


# Handler for new members
dispatcher.add_handler(MessageHandler(Filters.status_update.new_chat_members, new_user))


# With clean=True we ignore the previous messages since we started the bot
updater.start_polling(clean=True)

# Just wait
updater.idle()